<?php

////////////////////
// Initialisation //
////////////////////
// initialisation en désactivant l'affichage de débogage

use Symfony\Component\HttpFoundation\Request;

require_once __DIR__ . '/../vendor/autoload.php';

/////////////
// Routage //
/////////////

$requete = Request::createFromGlobals();
\TheFeed\Controleur\RouteurURL::traiterRequete($requete)->send();