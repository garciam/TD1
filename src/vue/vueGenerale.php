<?php

use TheFeed\Lib\ConnexionUtilisateur;

/**
 * @var string $pagetitle
 * @var string $cheminVueBody
 * @var String[][] $messagesFlash
 */
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\Generator\UrlGenerator;
use TheFeed\Lib\Conteneur;

/** @var UrlGenerator $generateurUrl */
$generateurUrl = Conteneur::recupererService("generateurUrl");
/** @var UrlHelper $assistantUrl */
$assistantUrl = Conteneur::recupererService("assistantUrl");
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <title><?= $pagetitle ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?php echo $assistantUrl->getAbsoluteUrl("../ressources/css/styles.css") ?>">
</head>

<body>
    <header>
        <div id="titre" class="center">
            <a href="<?= $generateurUrl->generate("afficherListe")?>"<span>The Feed</span></a>
            <nav>
                <a href="<?= $generateurUrl->generate("afficherListe")?>">Accueil</a>
                <?php
                if (!ConnexionUtilisateur::estConnecte()) {
                ?>
                    <a href="<?= $generateurUrl->generate("formulaireInscription")?>">Inscription</a>
                    <a href="<?= $generateurUrl->generate("connexion")?>">Connexion</a>
                <?php
                } else {
                    $idUtilisateurURL = rawurlencode(ConnexionUtilisateur::getIdUtilisateurConnecte());
                ?>
                    <a href="<?= $generateurUrl->generate("afficherPublicationsVariable", ["idUtilisateur" => ConnexionUtilisateur::getIdUtilisateurConnecte()]); ?>">Ma
                        page</a>
                    <a href="<?= $generateurUrl->generate("deconnexion")?>">Déconnexion</a>
                <?php } ?>
            </nav>
        </div>
    </header>
    <div id="flashes-container">
        <?php
        foreach (["success", "error"] as $type) {
            foreach ($messagesFlash[$type] as $messageFlash) {
        ?>
                <span class="flashes flashes-<?= $type ?>"><?= $messageFlash ?></span>
        <?php
            }
        }
        ?>
    </div>
    <?php
    require __DIR__ . "/{$cheminVueBody}";
    ?>
</body>

</html>