<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Test\Ensemble;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ControleurPublication extends ControleurGenerique
{
    public function __construct(ContainerInterface $container, private PublicationServiceInterface $publicationService)
    {
        parent::__construct($container);
    }

    #[Route(path: '/publications', name:'afficherListe', methods:["GET"])]
    #[Route(path: '/', name:'afficherListe', methods:["GET"])]


    public function afficherListe(): Response
    {
        $publications=$this->publicationService->recupererPublications();
        return ControleurPublication::afficherTwig("publication/feed.html.twig", ["publications" => $publications]);
    }
    #[Route(path: '/publications', name:'creerPublication', methods:["POST"])]
    public function creerDepuisFormulaire() : Response
    {
        $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
        $message = $_POST['message'];
        try {
            $this->publicationService->creerPublication($idUtilisateurConnecte,$message);
        }
        catch(ServiceException $e) {
            MessageFlash::ajouter("error",$e);
        }
        return ControleurPublication::rediriger('afficherListe');
    }


}