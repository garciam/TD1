<?php

namespace TheFeed\Controleur;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Configuration\Configuration;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\UtilisateurService;
use TheFeed\Service\UtilisateurServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ControleurUtilisateur extends ControleurGenerique
{
    public function __construct(private UtilisateurServiceInterface $utilisateurService, private PublicationServiceInterface $publicationService,ContainerInterface $container){
        parent::__construct($container);
    }


    public function afficherErreur($messageErreur = "", $controleur = ""): Response
    {
        return parent::afficherErreur($messageErreur, "utilisateur");
    }
    #[Route(path: '/utilisateurs/{idUtilisateur}/publications"', name:'afficherPublicationsVariable', methods:["GET"])]

public function afficherPublications($idUtilisateur): Response    {

        try {
        $user=$this->utilisateurService->recupererUtilisateurParId($idUtilisateur);
        $login=$user->getLogin();
        $publications=$this->publicationService->recupererPublicationsUtilisateur($idUtilisateur);
        return ControleurUtilisateur::afficherTwig("publication/page_perso.html.twig",["publications" => $publications,"login" => $login]);
        }
        catch(ServiceException $e) {
            MessageFlash::ajouter("error", $e);
            return ControleurUtilisateur::rediriger("afficherListe");
        }
    }
    #[Route(path: '/inscription', name:'formulaireInscription', methods:["GET"])]
    public function afficherFormulaireCreation(): Response
    {

        return ControleurUtilisateur::afficherTwig("utilisateur/inscription.html.twig");
    }

    #[Route(path: '/inscription', name:'inscrire', methods:["POST"])]
    public function creerDepuisFormulaire(): Response
    {        //Recupérer les différentes variables (login, mot de passe, adresse mail, données photo de profil...)
         $login = $_POST["login"] ?? null;
        $mdp = $_POST['mot-de-passe'] ?? null;
        $email=$_POST['email'] ?? null;
        $pdp=$_FILES['nom-photo-de-profil']?? null;

        try {
            $this->utilisateurService->creerUtilisateur($login,$mdp,$email,$pdp);
        }
        catch(ServiceException $e) {
            MessageFlash::ajouter("error", $e);
            return ControleurUtilisateur::rediriger("afficherListe");
        }
        MessageFlash::ajouter("success", "L'utilisateur a bien été créé !");
        return ControleurUtilisateur::rediriger("afficherListe");
    }
    #[Route(path: '/connexion', name:'connexion', methods:["GET"])]
    public function afficherFormulaireConnexion(): Response
    {
        return ControleurUtilisateur::afficherTwig('utilisateur/connexion.html.twig');
    }

    #[Route(path: '/connexion', name:'afficherFormulaireConnexion', methods:["POST"])]
    public function connecter(): Response
    {
        if (!(isset($_POST['login']) && isset($_POST['mot-de-passe']))) {
            MessageFlash::ajouter("error", "Login ou mot de passe manquant.");
            return ControleurUtilisateur::rediriger("afficherFormulaireConnexion");
        }
        $utilisateur = $_POST["login"] ?? null;
        $mdp = $_POST["mot-de-passe"] ?? null;

        try{
            $this->utilisateurService->connecter($utilisateur,$mdp);
            return ControleurUtilisateur::rediriger("afficherListe");

        }
        catch(ServiceException $e) {
            MessageFlash::ajouter("error", $e);
            return ControleurUtilisateur::rediriger("afficherFormulaireConnexion");

        }
    }
    #[Route(path: '/deconnexion', name:'deconnexion', methods:["GET"])]

    public function deconnecter(): Response
    {
        try{
            $this->utilisateurService->deconnecter();
            MessageFlash::ajouter("success", "L'utilisateur a bien été déconnecté.");
        }catch (ServiceException $e){
            MessageFlash::ajouter("error", $e);
        }
        return ControleurUtilisateur::rediriger("afficherListe");
    }
}
