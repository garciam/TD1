<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\RedirectResponse;
use TheFeed\Lib\Conteneur;
use TheFeed\Lib\MessageFlash;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Modele\Repository\PublicationRepositoryInterface;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ControleurGenerique {
    public function __construct(private ContainerInterface $container)
    {}

    protected   function afficherVue(string $cheminVue, array $parametres = []): Response
    {
        extract($parametres);
        $messagesFlash = MessageFlash::lireTousMessages();
        ob_start();
        require __DIR__ . "/../vue/$cheminVue";
        $corpsReponse = ob_get_clean();
        return new Response($corpsReponse);
   }

    // https://stackoverflow.com/questions/768431/how-do-i-make-a-redirect-in-php
    protected function rediriger(string $routeName, array $parametres=[]) : RedirectResponse
    {
        $temp=$this->container->get("url_generator")->generate($routeName,$parametres);
        return new RedirectResponse($temp);
    }

    public function afficherErreur($messageErreur = "", $statusCode = 400): Response
    {
        $reponse = ControleurGenerique::afficherVue('vueGenerale.php', [
            "pagetitle" => "Problème",
            "cheminVueBody" => "erreur.php",
            "messageErreur" => $messageErreur
        ]);
        $reponse->setStatusCode($statusCode);
        return $reponse;
    }
    protected function afficherTwig(string $cheminVue, array $parametres = []): Response
 {
     /** @var Environment $twig */
     $twig = $this->container->get("twig");
     $corpsReponse = $twig->render($cheminVue, $parametres);
     return new Response($corpsReponse);
 }

}