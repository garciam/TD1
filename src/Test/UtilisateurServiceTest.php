<?php
namespace TheFeed\Test;

use PHPUnit\Framework\TestCase;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\FileMovingServiceInterface;
use TheFeed\Service\UploadedFileMovingService;
use TheFeed\Service\UtilisateurService;

class UtilisateurServiceTest extends TestCase
{

    private $service;

    private $utilisateurRepositoryMock;

    //Dossier où seront déplacés les fichiers pendant les tests
    private  $dossierPhotoDeProfil = __DIR__."/asset/";

    private FileMovingServiceInterface $fileMovingService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->utilisateurRepositoryMock = $this->createMock(UtilisateurRepositoryInterface::class);
        $this->fileMovingService = $this->createMock(UploadedFileMovingService::class);
            mkdir($this->dossierPhotoDeProfil);
        $this->service = new UtilisateurService($this->utilisateurRepositoryMock,$this->dossierPhotoDeProfil,$this->fileMovingService);
    }

    public function testCreerUtilisateurPhotoDeProfil() {
        $donneesPhotoDeProfil = [];
        $donneesPhotoDeProfil["name"] = "test.png";
        $donneesPhotoDeProfil["tmp_name"] = "test.png";
        $this->utilisateurRepositoryMock->method("recupererParLogin")->willReturn(null);
        $this->utilisateurRepositoryMock->method("recupererParEmail")->willReturn(null);
        $this->utilisateurRepositoryMock->method("ajouter")->willReturnCallback(function ($utilisateur) use ($donneesPhotoDeProfil) {
            self::assertFileExists($this->dossierPhotoDeProfil. $donneesPhotoDeProfil["name"]);        });
        $this->service->creerUtilisateur("test", "TestMdp123", "test@example.com", $donneesPhotoDeProfil);
    }

    protected function tearDown(): void
    {
        //Nettoyage
        parent::tearDown();
        foreach(scandir($this->dossierPhotoDeProfil) as $file) {
            if ('.' === $file || '..' === $file) continue;
            unlink($this->dossierPhotoDeProfil.$file);
        }
        rmdir($this->dossierPhotoDeProfil);
    }

}

