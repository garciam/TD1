<?php

namespace TheFeed\Test;

use TheFeed\Modele\Repository\PublicationRepositoryInterface;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\PublicationService;
use Exception;
use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\assertEquals;

class PublicationServiceTest extends TestCase
{
    private $service;

    private $publicationRepositoryMock;

    private $utilisateurRepositoryMock;

    protected function setUp(): void
    {
        parent::setUp();
        $this->publicationRepositoryMock = $this->createMock(PublicationRepositoryInterface::class);
        $this->utilisateurRepositoryMock = $this->createMock(UtilisateurRepositoryInterface::class);
        $this->service = new PublicationService($this->publicationRepositoryMock, $this->utilisateurRepositoryMock);
    }
    public function testCreerPublicationUtilisateurInexistant(){
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Il faut être connecté pour publier un feed");
        $this->service->creerPublication("-1","test");
    }
    public function testCreerPublicationVide(){
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Le message ne peut pas être vide!");
        $this->service->creerPublication("1","");
    }
    public function testCreerPublicationTropGrande(){
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Le message ne peut pas dépasser 250 caractères!");
        $this->service->creerPublication("1",str_repeat("0", 255));
    }

    public function testNombrePublications(){
        $p=$this->service->recupererPublications();
        assertEquals(5,sizeof($p));
    }

    public function testNombrePublicationsUtilisateur(){
        $p=$this->service->recupererPublicationsUtilisateur(1);
        assertEquals(2,sizeof($p));
    }

    public function testNombrePublicationsUtilisateurInexistant(){
        $p=$this->service->recupererPublicationsUtilisateur(-1);
        assertEquals(0,sizeof($p));
    }


}