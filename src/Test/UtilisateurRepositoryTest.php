<?php
namespace TheFeed\Test;

use PHPUnit\Framework\TestCase;
use TheFeed\Modele\Repository\ConnexionBaseDeDonnees;
use TheFeed\Modele\Repository\ConnexionBaseDeDonneesInterface;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\PublicationServiceInterface;

class UtilisateurRepositoryTest extends TestCase
{
    private static UtilisateurRepositoryInterface  $utilisateurRepository;

    private static ConnexionBaseDeDonneesInterface $connexionBaseDeDonnees;
    private static PublicationServiceInterface $publicationRepository;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$connexionBaseDeDonnees = new ConnexionBaseDeDonnees(new ConfigurationBDDTestUnitaire());
        self::$utilisateurRepository = new UtilisateurRepository(self::$connexionBaseDeDonnees);
        self::$publicationRepository = new PublicationRepository(self::$publicationRepository);

    }

    protected function setUp(): void
    {
        parent::setUp();
        self::$connexionBaseDeDonnees->getPdo()->query("INSERT INTO 
                                                         utilisateurs (idUtilisateur, login, mdpHache, email, nomPhotoDeProfil) 
                                                         VALUES (1, 'test', 'test', 'test@example.com', 'test.png')");
        self::$connexionBaseDeDonnees->getPdo()->query("INSERT INTO 
                                                         utilisateurs (idUtilisateur, login, mdpHache, email, nomPhotoDeProfil) 
                                                         VALUES (2, 'test2', 'test2', 'test2@example.com', 'test2.png')");
        self::$connexionBaseDeDonnees->getPdo()->query("INSERT INTO 
                                                         publications
                                                         VALUES (1, 'test', '01/01/01', 1)");
    }

    public function testSimpleNombreUtilisateurs() {
        $this->assertCount(2, self::$utilisateurRepository->recuperer());
    }
    public function testSimpleNombrePublication() {
        $this->assertCount(1, self::$publicationRepository->recupererParClePrimaire(1));
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        self::$connexionBaseDeDonnees->getPdo()->query("DELETE FROM utilisateurs");
    }

}
