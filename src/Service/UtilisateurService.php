<?php

namespace TheFeed\Service;

use TheFeed\Controleur\ControleurUtilisateur;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;

class UtilisateurService implements UtilisateurServiceInterface

{

    public function __construct(private UtilisateurRepositoryInterface $utilisateurRepository, private string $dossierPhotoDeProfil, private FileMovingServiceInterface $fileMovingService){}

    public function creerUtilisateur($login, $motDePasse, $email, $nomPhotoDeProfil) : void {
        //TO-DO
        //Verifier que les attributs ne sont pas null
        //Verifier la taille du login
        //Verifier la validité du mot de passe
        //Verifier le format de l'adresse mail
        //Verifier que l'utilisateur n'existe pas déjà
        //Verifier que l'adresse mail n'est pas prise
        //Verifier extension photo de profil
        //Enregistrer la photo de profil
        //Chiffrer le mot de passe
        //Enregistrer l'utilisateur...

//        if ($login!=null && $motDePasse !=null && $email!=null && $nomPhotoDeProfil!=null)

        if (strlen($login) < 4 || strlen($login) > 20) {
            throw new ServiceException("Le login doit être compris entre 4 et 20 caractères!");

        }
        if (!preg_match("#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,20}$#", $motDePasse)) {
            throw new ServiceException("Mot de passe invalide!");

        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new ServiceException("L'adresse mail est incorrecte!");

        }

        $utilisateur = $this->utilisateurRepository->recupererParLogin($login);
        if ($utilisateur != null) {
            throw new ServiceException("Ce login est déjà pris!");

        }

        $utilisateur = $this->utilisateurRepository->recupererParEmail($email);
        if ($utilisateur != null) {
            throw new ServiceException("Un compte est déjà enregistré avec cette adresse mail!");

        }

        $mdpHache = MotDePasse::hacher($motDePasse);

        // Upload des photos de profil
        // Plus d'informations :
        // http://romainlebreton.github.io/R3.01-DeveloppementWeb/assets/tut4-complement.html

        // On récupère l'extension du fichier
        $explosion = explode('.', $nomPhotoDeProfil['name']);
        $fileExtension = end($explosion);
        if (!in_array($fileExtension, ['png', 'jpg', 'jpeg'])) {
            throw new ServiceException("La photo de profil n'est pas au bon format!");

        }
        // La photo de profil sera enregistrée avec un nom de fichier aléatoire
        $pictureName = uniqid() . '.' . $fileExtension;
        $from = $nomPhotoDeProfil['tmp_name'];
        $to = $this->dossierPhotoDeProfil . "/$pictureName";
        $this->fileMovingService->moveFile($from, $to);

        $utilisateur = Utilisateur::create($login, $mdpHache, $email, $pictureName);
        $this->utilisateurRepository->ajouter($utilisateur);
    }

    public function recupererUtilisateurParId($idUtilisateur, $autoriserNull = true) : ?Utilisateur {
        $utilisateur = $this->utilisateurRepository->recupererParClePrimaire($idUtilisateur);
     if(!$autoriserNull && $utilisateur==null) {
         throw new ServiceException("L'utilisateur n'existe pas");
     }
     return $utilisateur;
    }

    public function connecter($utilisateur,$mdp){
        $utilisateur= $this->utilisateurRepository->recupererParLogin($utilisateur);
        if ($utilisateur == null) {
            throw new ServiceException("Login inconnu");
        }
        if (!MotDePasse::verifier($mdp, $utilisateur->getMdpHache())) {
            throw new ServiceException("Mot de passe incorrect");

        }
        ConnexionUtilisateur::connecter($utilisateur->getIdUtilisateur());
    }

    public function deconnecter(){
        if (!ConnexionUtilisateur::estConnecte()) {
            throw new ServiceException("Utilisateur non connecté");
        }
        ConnexionUtilisateur::deconnecter();

    }
}