<?php

namespace TheFeed\Service;

use TheFeed\Modele\DataObject\Utilisateur;

interface UtilisateurServiceInterface
{
    public function creerUtilisateur($login, $motDePasse, $email, $nomPhotoDeProfil): void;

    public function recupererUtilisateurParId($idUtilisateur, $autoriserNull = true): ?Utilisateur;

    public function connecter($utilisateur, $mdp);

    public function deconnecter();
}